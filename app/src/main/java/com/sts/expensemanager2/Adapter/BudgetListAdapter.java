package com.sts.expensemanager2.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sts.expensemanager2.Model.BudgetModel;
import com.sts.expensemanager2.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mac on 2/16/17.
 */

public class BudgetListAdapter extends RecyclerView.Adapter<BudgetListAdapter.ViewHolder> {

    private Context ctx;
    private List<BudgetModel> mDataset = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView account_type,amount,category_type,payment_method,period,from_date,to_date;

        public ViewHolder(View v) {
            super(v);

            account_type = (TextView) v.findViewById(R.id.account_type);
            amount = (TextView) v.findViewById(R.id.amount);
            category_type = (TextView) v.findViewById(R.id.category_type);
            payment_method = (TextView) v.findViewById(R.id.payment_method);
            period = (TextView) v.findViewById(R.id.period);
            from_date = (TextView) v.findViewById(R.id.from_date);
            to_date = (TextView) v.findViewById(R.id.to_date);

            ctx = v.getContext();
        }
    }
    public BudgetListAdapter(List<BudgetModel> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public BudgetListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.budget_list_adapter, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(BudgetListAdapter.ViewHolder holder, int position) {

        holder.account_type.setText(mDataset.get(position).getBudgetAccount());
        holder.amount.setText(mDataset.get(position).getBudgetAmount());
        holder.category_type.setText(mDataset.get(position).getBudgetCategory());
        holder.payment_method.setText(mDataset.get(position).getBudgetPaymentMethod());
        holder.period.setText(mDataset.get(position).getBudgetPeriod());
        holder.from_date.setText(mDataset.get(position).getBudgetFromDate());
        holder.to_date.setText(mDataset.get(position).getBudgetToDate());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void refreshedData(List<BudgetModel> list){
        mDataset.clear();
        mDataset.addAll(list);
        notifyDataSetChanged();
    }
}
