package com.sts.expensemanager2.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sts.expensemanager2.Model.ExpenseModel;
import com.sts.expensemanager2.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mac on 2/19/17.
 */

public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.ViewHolder> {

    private Context ctx;
    private List<ExpenseModel> mDataset = new ArrayList<>();
    RecyclerView recyclerView;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView report_amount_text,report_category_text,report_month_text;

        public ViewHolder(View v) {
            super(v);

            report_amount_text = (TextView) v.findViewById(R.id.report_amount_text);
            report_category_text = (TextView) v.findViewById(R.id.report_category_text);
            report_month_text = (TextView) v.findViewById(R.id.report_month_text);

            ctx = v.getContext();
        }
    }
    public ReportListAdapter(List<ExpenseModel> myDataset) {
        mDataset = myDataset;
    }


    @Override
    public ReportListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_list_adapter, parent, false);
        ReportListAdapter.ViewHolder vh = new ReportListAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ReportListAdapter.ViewHolder holder, int position) {

        holder.report_amount_text.setText(mDataset.get(position).getAmount());
        holder.report_category_text.setText(mDataset.get(position).getCategory());
        holder.report_month_text.setText(mDataset.get(position).getThisMonth());
    }

    @Override
    public int getItemCount() {

        return mDataset.size();
    }
}
