package com.sts.expensemanager2.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sts.expensemanager2.Model.IncomeModel;
import com.sts.expensemanager2.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mac on 2/22/17.
 */

public class IncomeListAdapter extends RecyclerView.Adapter<IncomeListAdapter.ViewHolder> {

    private Context ctx;
    private List<IncomeModel> mDataset = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView amount_text_show, payer_text, category_text, payment_method_text, payment_status_text, reference_text, day_text, month_text, year_text;

        public ViewHolder(View v) {
            super(v);

            amount_text_show = (TextView) v.findViewById(R.id.amount_text_show);
            payer_text = (TextView) v.findViewById(R.id.payer_text);
            category_text = (TextView) v.findViewById(R.id.category_text_show);
            payment_method_text = (TextView) v.findViewById(R.id.payment_method_text_show);
            payment_status_text = (TextView) v.findViewById(R.id.payment_status_text_show);
            reference_text = (TextView) v.findViewById(R.id.reference_text);
            day_text = (TextView) v.findViewById(R.id.day_text_show);
            month_text = (TextView) v.findViewById(R.id.month_text_show);
            year_text = (TextView) v.findViewById(R.id.year_text_show);

            ctx = v.getContext();
        }
    }
    public IncomeListAdapter(List<IncomeModel> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public IncomeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.income_list_adapter, parent, false);
        IncomeListAdapter.ViewHolder vh = new IncomeListAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(IncomeListAdapter.ViewHolder holder, int position) {

        holder.amount_text_show.setText(mDataset.get(position).getAmount());
        holder.payer_text.setText(mDataset.get(position).getPayer());
        holder.category_text.setText(mDataset.get(position).getCategory());
        holder.payment_method_text.setText(mDataset.get(position).getPaymentMethod());
        holder.payment_status_text.setText(mDataset.get(position).getPaymentStatus());
        holder.reference_text.setText(mDataset.get(position).getRefCheck());
        holder.day_text.setText(mDataset.get(position).getToDays()+"/");
        holder.month_text.setText(mDataset.get(position).getThisMonth()+"/");
        holder.year_text.setText(mDataset.get(position).getThisYear());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
