package com.sts.expensemanager2;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.activeandroid.util.SQLiteUtils;
import com.sts.expensemanager2.Model.ExpenseModel;
import com.sts.expensemanager2.Model.IncomeModel;
import com.sts.expensemanager2.UI.BudgetActivity;
import com.sts.expensemanager2.UI.ExpenseActivity;
import com.sts.expensemanager2.UI.IncomeActivity;
import com.sts.expensemanager2.UI.ReportActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    LinearLayout income_activity,expense_activity,budget_activity,report_activity;
    TextView current_balance,todays_expense,monthly_expense,monthly_income,yearly_expense,weekly_expense;
    List<IncomeModel> incomeModel = new ArrayList<IncomeModel>();
    List<ExpenseModel> expenseModels = new ArrayList<ExpenseModel>();
    int expenseSum = 0;
    int incomeSum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prepare_stage();

        incomeModel = new Select().from(IncomeModel.class).execute();
        for(int i=0; i<incomeModel.size(); i++){
            incomeSum = incomeSum + Integer.parseInt(incomeModel.get(i).getAmount());
            //Toast.makeText(MainActivity.this, incomeModel.get(i).getAmount(), Toast.LENGTH_SHORT).show();
        }

        expenseModels = new Select().from(ExpenseModel.class).execute();
        for(int i=0; i<expenseModels.size(); i++){
            expenseSum = expenseSum + Integer.parseInt(expenseModels.get(i).getAmount());
        }

        //Toast.makeText(MainActivity.this, " Income Sum "+incomeSum, Toast.LENGTH_SHORT).show();
        //Toast.makeText(MainActivity.this, " Expense Sum "+expenseSum, Toast.LENGTH_SHORT).show();
        int currentBalance = incomeSum - expenseSum;
        current_balance.setText(currentBalance+".00");
        //Toast.makeText(MainActivity.this, " Current Balance "+currentBalance, Toast.LENGTH_SHORT).show();

        date_calculation();

    }

    public void date_calculation(){

        Calendar myDate = Calendar.getInstance();
        int toDays = myDate.get(Calendar.DAY_OF_MONTH);
        int cal = myDate.get(Calendar.MONTH);
        int thisMonth = cal + 1;
        int thisYear = myDate.get(Calendar.YEAR);


        //Today's expenses calculation and show the result
        expenseModels = new Select().from(ExpenseModel.class).where("toDay = ?",toDays).execute();
        int toDaysExpense = 0;
        for(int i=0; i<expenseModels.size(); i++){
            toDaysExpense = toDaysExpense + Integer.parseInt(expenseModels.get(i).getAmount());
            //Toast.makeText(MainActivity.this, i+" Today's Expense list "+expenseModels.get(i).getAmount(), Toast.LENGTH_SHORT).show();
        }
        //Toast.makeText(MainActivity.this, "Today's Total Cost "+toDaysExpense, Toast.LENGTH_SHORT).show();
        todays_expense.setText(toDaysExpense+".00");

        //Weekly Expense calculation and show the value
        Calendar myDate1 = Calendar.getInstance();
        myDate1.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        myDate1.add(Calendar.DAY_OF_YEAR, -7);
        int dy = myDate1.get(Calendar.DAY_OF_MONTH);
        expenseModels = new Select().from(ExpenseModel.class).where("toDay BETWEEN "+dy+" AND "+toDays).execute();
        int sun = 0;
        for(int i=0; i<expenseModels.size(); i++){
            sun = sun + Integer.parseInt(expenseModels.get(i).getAmount());
        }
        weekly_expense.setText(sun+".00");

        //Monthly expenses calculation and show the result
        expenseModels = new Select().from(ExpenseModel.class).where("thisMonth = ?", thisMonth).execute();
        int thisMonthExpense = 0;
        for(int i=0; i<expenseModels.size(); i++){
            thisMonthExpense = thisMonthExpense + Integer.parseInt(expenseModels.get(i).getAmount());
        }
        //Toast.makeText(MainActivity.this, "This Month Total Cost "+thisMonthExpense, Toast.LENGTH_SHORT).show();
        monthly_expense.setText(thisMonthExpense+".00");

        //Yearly expenses calculation and show the result
        expenseModels = new Select().from(ExpenseModel.class).where("thisYear = ?", thisYear).execute();
        int thisYearExpense = 0;
        for(int i=0; i<expenseModels.size(); i++){
            thisYearExpense = thisYearExpense + Integer.parseInt(expenseModels.get(i).getAmount());
        }
        //Toast.makeText(MainActivity.this, "This Year Total Cost "+thisYearExpense, Toast.LENGTH_SHORT).show();
        yearly_expense.setText(thisYearExpense+".00");


        //Monthly income calculation and show the result
        incomeModel = new Select().from(IncomeModel.class).where("thisMonth = ?", thisMonth).execute();
        int thisMonthIncome = 0;
        for(int i=0; i<incomeModel.size(); i++){
            thisMonthIncome = thisMonthIncome + Integer.parseInt(incomeModel.get(i).getAmount());
        }
        //Toast.makeText(MainActivity.this, "This Month Total Income "+thisMonthIncome, Toast.LENGTH_SHORT).show();
        monthly_income.setText(thisMonthIncome+".00");

    }

    public void prepare_stage(){

        weekly_expense = (TextView) findViewById(R.id.weekly_expense);
        monthly_income = (TextView) findViewById(R.id.monthly_income);
        todays_expense = (TextView) findViewById(R.id.todays_expense);
        monthly_expense = (TextView) findViewById(R.id.monthly_expense);
        yearly_expense = (TextView) findViewById(R.id.yearly_expense);
        current_balance = (TextView) findViewById(R.id.current_balance);
        income_activity = (LinearLayout) findViewById(R.id.income_activity);
        income_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(MainActivity.this, IncomeActivity.class);
                startActivity(out);
            }
        });

        expense_activity = (LinearLayout) findViewById(R.id.expense_activity);
        expense_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(MainActivity.this, ExpenseActivity.class);
                startActivity(out);
            }
        });

        budget_activity = (LinearLayout) findViewById(R.id.budget_activity);
        budget_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(MainActivity.this, BudgetActivity.class);
                startActivity(out);
            }
        });
        report_activity = (LinearLayout) findViewById(R.id.report_activity);
        report_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(MainActivity.this, ReportActivity.class);
                startActivity(out);
            }
        });
    }
}
