package com.sts.expensemanager2.Model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by mac on 2/15/17.
 */
@Table(name = "expense_amount", id = "_id")
public class ExpenseModel extends Model{

    @Column
    public String amount;
    @Column
    public String payee;
    @Column
    public String category;
    @Column
    public String paymentMethod;
    @Column
    public String paymentStatus;
    @Column
    public String refCheck;
    @Column
    public String toDay;
    @Column
    public String thisMonth;
    @Column
    public String thisYear;

    public ExpenseModel() {
    }

    public ExpenseModel(String amount, String payee, String category, String paymentMethod, String paymentStatus, String refCheck, String toDay, String thisMonth, String thisYear) {
        this.amount = amount;
        this.payee = payee;
        this.category = category;
        this.paymentMethod = paymentMethod;
        this.paymentStatus = paymentStatus;
        this.refCheck = refCheck;
        this.toDay = toDay;
        this.thisMonth = thisMonth;
        this.thisYear = thisYear;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getRefCheck() {
        return refCheck;
    }

    public void setRefCheck(String refCheck) {
        this.refCheck = refCheck;
    }

    public String getToDay() {
        return toDay;
    }

    public void setToDay(String toDay) {
        this.toDay = toDay;
    }

    public String getThisMonth() {
        return thisMonth;
    }

    public void setThisMonth(String thisMonth) {
        this.thisMonth = thisMonth;
    }

    public String getThisYear() {
        return thisYear;
    }

    public void setThisYear(String thisYear) {
        this.thisYear = thisYear;
    }
}
