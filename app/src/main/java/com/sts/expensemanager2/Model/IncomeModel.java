package com.sts.expensemanager2.Model;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

/**
 * Created by mac on 2/14/17.
 */
@Table(name = "income_amount", id = "_id")
public class IncomeModel extends Model {

    @Column
    public String amount;
    @Column
    public String payer;
    @Column
    public String category;
    @Column
    public String paymentMethod;
    @Column
    public String paymentStatus;
    @Column
    public String refCheck;
    @Column
    public String toDays;
    @Column
    public String thisMonth;
    @Column
    public String thisYear;

    public IncomeModel() {
    }

    public IncomeModel(String amount, String payer, String category, String paymentMethod, String paymentStatus, String refCheck, String toDays, String thisMonth, String thisYear) {
        this.amount = amount;
        this.payer = payer;
        this.category = category;
        this.paymentMethod = paymentMethod;
        this.paymentStatus = paymentStatus;
        this.refCheck = refCheck;
        this.toDays = toDays;
        this.thisMonth = thisMonth;
        this.thisYear = thisYear;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayer() {
        return payer;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getRefCheck() {
        return refCheck;
    }

    public void setRefCheck(String refCheck) {
        this.refCheck = refCheck;
    }

    public String getToDays() {
        return toDays;
    }

    public void setToDays(String toDays) {
        this.toDays = toDays;
    }

    public String getThisMonth() {
        return thisMonth;
    }

    public void setThisMonth(String thisMonth) {
        this.thisMonth = thisMonth;
    }

    public String getThisYear() {
        return thisYear;
    }

    public void setThisYear(String thisYear) {
        this.thisYear = thisYear;
    }
}
