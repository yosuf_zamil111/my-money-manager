package com.sts.expensemanager2.Model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by mac on 2/15/17.
 */
@Table(name = "budget_amount", id = "_id")
public class BudgetModel extends Model {

    @Column
    public String budgetAccount;
    @Column
    public String budgetCategory;
    @Column
    public String budgetPaymentMethod;
    @Column
    public String budgetPeriod;
    @Column
    public String budgetFromDate;
    @Column
    public String budgetToDate;
    @Column
    public String budgetAmount;
    @Column
    public String budgetDescription;

    public BudgetModel() {
    }

    public BudgetModel(String budgetAccount, String budgetCategory, String budgetPaymentMethod, String budgetPeriod, String budgetFromDate, String budgetToDate, String budgetAmount, String budgetDescription) {
        this.budgetAccount = budgetAccount;
        this.budgetCategory = budgetCategory;
        this.budgetPaymentMethod = budgetPaymentMethod;
        this.budgetPeriod = budgetPeriod;
        this.budgetFromDate = budgetFromDate;
        this.budgetToDate = budgetToDate;
        this.budgetAmount = budgetAmount;
        this.budgetDescription = budgetDescription;
    }


    public String getBudgetAccount() {
        return budgetAccount;
    }

    public void setBudgetAccount(String budgetAccount) {
        this.budgetAccount = budgetAccount;
    }

    public String getBudgetCategory() {
        return budgetCategory;
    }

    public void setBudgetCategory(String budgetCategory) {
        this.budgetCategory = budgetCategory;
    }

    public String getBudgetPaymentMethod() {
        return budgetPaymentMethod;
    }

    public void setBudgetPaymentMethod(String budgetPaymentMethod) {
        this.budgetPaymentMethod = budgetPaymentMethod;
    }

    public String getBudgetPeriod() {
        return budgetPeriod;
    }

    public void setBudgetPeriod(String budgetPeriod) {
        this.budgetPeriod = budgetPeriod;
    }

    public String getBudgetFromDate() {
        return budgetFromDate;
    }

    public void setBudgetFromDate(String budgetFromDate) {
        this.budgetFromDate = budgetFromDate;
    }

    public String getBudgetToDate() {
        return budgetToDate;
    }

    public void setBudgetToDate(String budgetToDate) {
        this.budgetToDate = budgetToDate;
    }

    public String getBudgetAmount() {
        return budgetAmount;
    }

    public void setBudgetAmount(String budgetAmount) {
        this.budgetAmount = budgetAmount;
    }

    public String getBudgetDescription() {
        return budgetDescription;
    }

    public void setBudgetDescription(String budgetDescription) {
        this.budgetDescription = budgetDescription;
    }
}
