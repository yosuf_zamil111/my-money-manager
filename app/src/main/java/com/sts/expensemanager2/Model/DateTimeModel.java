package com.sts.expensemanager2.Model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by mac on 2/16/17.
 */
@Table(name = "date_time_table", id = "_id")
public class DateTimeModel extends Model{


    @Column
    public int toDay;
    @Column
    public int thisMonth;
    @Column
    public int thisYear;

    public DateTimeModel() {
    }

    public DateTimeModel(int toDay, int thisMonth, int thisYear) {
        this.toDay = toDay;
        this.thisMonth = thisMonth;
        this.thisYear = thisYear;
    }


    public int getToDay() {
        return toDay;
    }

    public void setToDay(int toDay) {
        this.toDay = toDay;
    }

    public int getThisMonth() {
        return thisMonth;
    }

    public void setThisMonth(int thisMonth) {
        this.thisMonth = thisMonth;
    }

    public int getThisYear() {
        return thisYear;
    }

    public void setThisYear(int thisYear) {
        this.thisYear = thisYear;
    }
}
