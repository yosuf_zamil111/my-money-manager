package com.sts.expensemanager2.UI;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sts.expensemanager2.Model.BudgetModel;
import com.sts.expensemanager2.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BudgetActivity extends AppCompatActivity {

    Spinner budget_account_array,budget_category_array,budget_paymentMethod_array,budget_period_array;
    Button cancel_budgetBtt,save_budgetBtt;
    EditText budget_amount_text,budget_description_text;
    TextView budget_from_date, budget_to_date;

    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget);

        prepare_stage();

    }

    public void prepare_stage(){

        budget_account_array = (Spinner) findViewById(R.id.budget_account_array);
        budget_category_array = (Spinner) findViewById(R.id.budget_category_array);
        budget_paymentMethod_array = (Spinner) findViewById(R.id.budget_paymentMethod_array);
        budget_period_array = (Spinner) findViewById(R.id.budget_period_array);
        budget_from_date = (TextView) findViewById(R.id.budget_from_date);
        budget_to_date = (TextView) findViewById(R.id.budget_to_date);
        cancel_budgetBtt = (Button) findViewById(R.id.cancel_budgetBtt);
        save_budgetBtt = (Button) findViewById(R.id.save_budgetBtt);
        budget_amount_text = (EditText) findViewById(R.id.budget_amount_text);
        budget_description_text = (EditText) findViewById(R.id.budget_description_text);

        budget_account_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.budget_account_array)));

        budget_category_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.budget_category_array)));

        budget_paymentMethod_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.budget_paymentMethod_array)));

        budget_period_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.budget_period_array)));

        budget_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setFromDateField();

            }
        });

        budget_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setToDateField();

            }
        });

        cancel_budgetBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save_budgetBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean statusChk = true;

                if(TextUtils.isEmpty(budget_amount_text.getText())){
                    Toast.makeText(BudgetActivity.this,"Please put amount field",Toast.LENGTH_LONG).show();
                    return;
                }

                if(statusChk == true){
                    try{
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/M/yyyy");

                        String str1 = budget_from_date.getText().toString();
                        Date date1 = formatter.parse(str1);

                        String str2 = budget_to_date.getText().toString();
                        Date date2 = formatter.parse(str2);

                        if (date1.compareTo(date2)<0)
                        {
                            //System.out.println("date2 is Greater than my date1");
                            Toast.makeText(BudgetActivity.this, "Successfully Added Your New Budget.", Toast.LENGTH_SHORT).show();
                            addBudgetData();
                            Intent out = new Intent(BudgetActivity.this, BudgetListActivity.class);
                            startActivity(out);
                        }
                        else {
                            Toast.makeText(BudgetActivity.this,"Please put valid date field",Toast.LENGTH_LONG).show();
                        }

                    }catch (ParseException e1){
                        e1.printStackTrace();
                    }
                }
               /* else {
                    addBudgetData();
                    Toast.makeText(BudgetActivity.this, "Successfully Added Your New Budget.", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(BudgetActivity.this, budgetModel.getBudgetAmount(), Toast.LENGTH_SHORT).show();
                    //Toast.makeText(BudgetActivity.this, budgetModel.getBudgetFromDate(), Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }

    public void addBudgetData(){

        BudgetModel budgetModel = new BudgetModel();

        budgetModel.setBudgetAccount(budget_account_array.getSelectedItem().toString());
        budgetModel.setBudgetCategory(budget_category_array.getSelectedItem().toString());
        budgetModel.setBudgetPaymentMethod(budget_paymentMethod_array.getSelectedItem().toString());
        budgetModel.setBudgetPeriod(budget_period_array.getSelectedItem().toString());
        budgetModel.setBudgetFromDate(budget_from_date.getText().toString());
        budgetModel.setBudgetToDate(budget_to_date.getText().toString());
        budgetModel.setBudgetAmount(budget_amount_text.getText().toString());
        budgetModel.setBudgetDescription(budget_description_text.getText().toString());

        budgetModel.save();

        //Toast.makeText(BudgetActivity.this, budgetModel.getBudgetAmount()+" and "+budgetModel.getBudgetToDate(), Toast.LENGTH_SHORT).show();

    }

    private void setFromDateField() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mFromDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(BudgetActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                        budget_from_date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, mYear, mMonth, mFromDay);
        datePickerDialog.show();
    }

    private void setToDateField() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mToDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog
        datePickerDialog = new DatePickerDialog(BudgetActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                        budget_to_date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, mYear, mMonth, mToDay);
        datePickerDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_data:
                Intent out = new Intent(BudgetActivity.this, BudgetListActivity.class);
                startActivity(out);
                Toast.makeText(getApplicationContext(),"Show Budget List.",Toast.LENGTH_LONG).show();
                return true;
           /* case R.id.item2:
                return true;
            case R.id.item3:
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
