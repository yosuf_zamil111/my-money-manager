package com.sts.expensemanager2.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Time;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sts.expensemanager2.MainActivity;
import com.sts.expensemanager2.Model.ExpenseModel;
import com.sts.expensemanager2.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ExpenseActivity extends AppCompatActivity {

    TextView time_and_date;
    EditText expense_text,expense_check_text;
    Spinner expense_payee_array,expense_category_array,expense_paymentMethod_array,expense_status_array;
    Button save_expenseBtt;

    int toDay;
    int thisMonth;
    int thisYear;

    ImageView show_calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense);

        Calendar myDate = Calendar.getInstance();
        toDay = myDate.get(Calendar.DAY_OF_MONTH);
        int cal = myDate.get(Calendar.MONTH);
        thisYear = myDate.get(Calendar.YEAR);
        thisMonth = cal + 1;

        //Toast.makeText(ExpenseActivity.this, "Today "+toDay, Toast.LENGTH_SHORT).show();
        //Toast.makeText(ExpenseActivity.this, "Month "+thisMonth, Toast.LENGTH_SHORT).show();
        //Toast.makeText(ExpenseActivity.this, "Year "+thisYear, Toast.LENGTH_SHORT).show();

        prepare_stage();

    }

    public void prepare_stage(){

        show_calculator = (ImageView) findViewById(R.id.show_calculator);
        show_calculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(ExpenseActivity.this, CalculatorActivity.class);
                startActivity(out);
            }
        });

        time_and_date = (TextView) findViewById(R.id.time_and_date);
        String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
        time_and_date.setText(mydate.trim());


        expense_text = (EditText) findViewById(R.id.expense_text);
        expense_payee_array = (Spinner) findViewById(R.id.expense_payee_array);
        expense_category_array = (Spinner) findViewById(R.id.expense_category_array);
        expense_paymentMethod_array = (Spinner) findViewById(R.id.expense_paymentMethod_array);
        expense_status_array = (Spinner) findViewById(R.id.expense_status_array);
        expense_check_text = (EditText) findViewById(R.id.expense_check_text);
        save_expenseBtt = (Button) findViewById(R.id.save_expenseBtt);

        expense_payee_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.expense_payee_array)));
        expense_category_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.expense_category_array)));
        expense_paymentMethod_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.expense_paymentMethod_array)));
        expense_status_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.expense_status_array)));

        save_expenseBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(expense_text.getText())){
                    Toast.makeText(ExpenseActivity.this,"Please put amount field",Toast.LENGTH_LONG).show();
                    return;
                }
                else {
                    addingExpense();
                    Toast.makeText(ExpenseActivity.this, "Successfully Added Your Expense Data.", Toast.LENGTH_SHORT).show();
                   //Toast.makeText(ExpenseActivity.this, expenseModel.getCategory(), Toast.LENGTH_SHORT).show();
                    Intent out = new Intent(ExpenseActivity.this, MainActivity.class);
                    startActivity(out);
                }
            }
        });
    }

    public void addingExpense(){

        ExpenseModel expenseModel = new ExpenseModel();

        final String payer = expense_payee_array.getSelectedItem().toString().trim();
        final String category = expense_category_array.getSelectedItem().toString().trim();
        final String method = expense_paymentMethod_array.getSelectedItem().toString().trim();
        final String status = expense_status_array.getSelectedItem().toString().trim();

        expenseModel.setAmount(expense_text.getText().toString());
        expenseModel.setPayee(payer);
        expenseModel.setCategory(category);
        expenseModel.setPaymentMethod(method);
        expenseModel.setPaymentStatus(status);
        expenseModel.setRefCheck(expense_check_text.getText().toString());
        expenseModel.setToDay(""+toDay);
        expenseModel.setThisMonth(""+thisMonth);
        expenseModel.setThisYear(""+thisYear);

        expenseModel.save();

        /*Toast.makeText(ExpenseActivity.this, expenseModel.getToDay(), Toast.LENGTH_SHORT).show();
        Toast.makeText(ExpenseActivity.this, expenseModel.getThisMonth(), Toast.LENGTH_SHORT).show();
        Toast.makeText(ExpenseActivity.this, expenseModel.getThisYear(), Toast.LENGTH_SHORT).show();*/

    }
}
