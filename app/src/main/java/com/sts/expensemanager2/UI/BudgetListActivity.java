package com.sts.expensemanager2.UI;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.sts.expensemanager2.Adapter.BudgetListAdapter;
import com.sts.expensemanager2.Model.BudgetModel;
import com.sts.expensemanager2.R;

import java.util.ArrayList;
import java.util.List;

public class BudgetListActivity extends AppCompatActivity {

    LinearLayout budget_details, budget_list;
    TextView account_details, amount_details, category_type_details, payment_method_details, period_details, from_date_details, to_date_details, description_details;

    RecyclerView recycler_view;
    BudgetListAdapter budgetListAdapter;
    List<BudgetModel> budgetModels = new ArrayList<BudgetModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_list);

        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(layoutManager);

        //budgetModels = new Select().from(BudgetModel.class).execute();

        budgetListAdapter = new BudgetListAdapter(budgetModels);

        recycler_view.setAdapter(budgetListAdapter);
        new AsyncTaskRunner().execute();
        prepare_stage();

        recycler_view.addOnItemTouchListener(new RecyclerTouchListener(BudgetListActivity.this, recycler_view, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //Toast.makeText(ReportActivity.this, ""+position, Toast.LENGTH_SHORT).show();
                budget_list.setVisibility(View.GONE);
                budget_details.setVisibility(View.VISIBLE);

                load_budget_details_data(position);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    public void load_budget_details_data(int pos){

        account_details.setText(budgetModels.get(pos).getBudgetAccount());
        amount_details.setText(budgetModels.get(pos).getBudgetAmount());
        category_type_details.setText(budgetModels.get(pos).getBudgetCategory());
        payment_method_details.setText(budgetModels.get(pos).getBudgetPaymentMethod());
        period_details.setText(budgetModels.get(pos).getBudgetPeriod());
        from_date_details.setText(budgetModels.get(pos).getBudgetFromDate());
        to_date_details.setText(budgetModels.get(pos).getBudgetToDate());
        description_details.setText(budgetModels.get(pos).getBudgetDescription());

    }

    public void prepare_stage(){
        budget_details = (LinearLayout) findViewById(R.id.budget_details);
        budget_list = (LinearLayout) findViewById(R.id.budget_list);

        account_details = (TextView) findViewById(R.id.account_details);
        amount_details = (TextView) findViewById(R.id.amount_details);
        category_type_details = (TextView) findViewById(R.id.category_type_details);
        payment_method_details = (TextView) findViewById(R.id.payment_method_details);
        period_details = (TextView) findViewById(R.id.period_details);
        from_date_details = (TextView) findViewById(R.id.from_date_details);
        to_date_details = (TextView) findViewById(R.id.to_date_details);
        description_details = (TextView) findViewById(R.id.description_details);

    }

    @Override
    public void onBackPressed() {
        if(budget_details.getVisibility() == View.VISIBLE){
            budget_details.setVisibility(View.GONE);
            budget_list.setVisibility(View.VISIBLE);
        }else {
            finish();
        }
    }

    private class AsyncTaskRunner extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected Void doInBackground(Void... params) {

            budgetModels = new Select().from(BudgetModel.class).execute();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            budgetListAdapter.refreshedData(budgetModels);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(BudgetListActivity.this, "Progress Running", "Please Wait for a while.");
        }

        @Override
        protected void onProgressUpdate(Void... text) {

        }
    }
}
