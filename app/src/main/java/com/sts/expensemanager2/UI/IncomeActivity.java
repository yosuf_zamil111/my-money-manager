package com.sts.expensemanager2.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.sts.expensemanager2.Adapter.IncomeListAdapter;
import com.sts.expensemanager2.MainActivity;
import com.sts.expensemanager2.Model.IncomeModel;
import com.sts.expensemanager2.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class IncomeActivity extends AppCompatActivity {

    IncomeModel incomeModel = new IncomeModel();

    TextView time_and_date;
    EditText income_text,check_text;
    Spinner payer_array,category_array,method_array,status_array;
    Button save_incomeBtt;

    int toDay;
    int thisMonth;
    int thisYear;

    ImageView show_calculator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_income);


        Calendar myDate = Calendar.getInstance();
        toDay = myDate.get(Calendar.DAY_OF_MONTH);
        int cal = myDate.get(Calendar.MONTH);
        thisYear = myDate.get(Calendar.YEAR);
        thisMonth = cal + 1;

        prepare_stage();

    }

    public void prepare_stage(){

        show_calculator = (ImageView) findViewById(R.id.show_calculator);
        show_calculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(IncomeActivity.this, CalculatorActivity.class);
                //out.putExtra("results", result);
                startActivity(out);
            }
        });

        time_and_date = (TextView) findViewById(R.id.time_and_date);
        String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
        time_and_date.setText(mydate.trim());

        income_text = (EditText) findViewById(R.id.income_text);
        payer_array = (Spinner) findViewById(R.id.payer_array);
        category_array = (Spinner) findViewById(R.id.category_array);
        method_array = (Spinner) findViewById(R.id.method_array);
        status_array = (Spinner) findViewById(R.id.status_array);
        check_text = (EditText) findViewById(R.id.check_text);

        payer_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.payer_array)));
        category_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.category_array)));
        method_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.method_array)));
        status_array.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.status_array)));

        save_incomeBtt = (Button) findViewById(R.id.save_incomeBtt);
        save_incomeBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(income_text.getText())){
                    Toast.makeText(IncomeActivity.this,"Please put amount field",Toast.LENGTH_LONG).show();
                    return;
                }
                else {
                    save_income_data();
                    Toast.makeText(IncomeActivity.this, "Successfully Added Your Income Data.", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(IncomeActivity.this, incomeModel.getCategory(), Toast.LENGTH_SHORT).show();
                    Intent out = new Intent(IncomeActivity.this, MainActivity.class);
                    startActivity(out);
                }
            }
        });

    }

    public void save_income_data(){

        final String payer = payer_array.getSelectedItem().toString().trim();
        final String category = category_array.getSelectedItem().toString().trim();
        final String method = method_array.getSelectedItem().toString().trim();
        final String status = status_array.getSelectedItem().toString().trim();

        incomeModel.setAmount(income_text.getText().toString());
        incomeModel.setPayer(payer);
        incomeModel.setCategory(category);
        incomeModel.setPaymentMethod(method);
        incomeModel.setPaymentStatus(status);
        incomeModel.setRefCheck(check_text.getText().toString());
        incomeModel.setToDays(""+toDay);
        incomeModel.setThisMonth(""+thisMonth);
        incomeModel.setThisYear(""+thisYear);

        incomeModel.save();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_income, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.income_data:
                Intent out = new Intent(IncomeActivity.this, IncomeListActivity.class);
                startActivity(out);
                Toast.makeText(getApplicationContext(),"Show Income List.",Toast.LENGTH_LONG).show();
                return true;
            /*case R.id.item2:
                return true;
            case R.id.item3:
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
