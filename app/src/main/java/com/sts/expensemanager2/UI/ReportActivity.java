package com.sts.expensemanager2.UI;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.sts.expensemanager2.Adapter.ReportListAdapter;
import com.sts.expensemanager2.Model.ExpenseModel;
import com.sts.expensemanager2.Model.IncomeModel;
import com.sts.expensemanager2.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReportActivity extends AppCompatActivity {

    private RecyclerView recycler_view;
    ReportListAdapter reportListAdapter;
    LinearLayout report_layout, details_layout;
    TextView amount_text, payee_text, category_text, payment_method_text, payment_status_text, day_text, month_text, year_text;

    int toDay;
    int thisMonth;
    int thisYear;

    private TextView empty_list_show;

    LinearLayout weekly_report,monthly_report,all_report,todays_report;
    TextView income_total,expense_total,cash_total;
    List<IncomeModel> incomeModel = new ArrayList<IncomeModel>();
    List<ExpenseModel> expenseModels = new ArrayList<ExpenseModel>();
    int expenseSum = 0;
    int incomeSum = 0;
    int currentBalance = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        Calendar myDate = Calendar.getInstance();
        toDay = myDate.get(Calendar.DAY_OF_MONTH);
        int cal = myDate.get(Calendar.MONTH);
        thisYear = myDate.get(Calendar.YEAR);
        thisMonth = cal + 1;

        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(manager);

        //All expenses list of report shown by Default
        expenseModels = new Select().from(ExpenseModel.class).execute();
        reportListAdapter = new ReportListAdapter(expenseModels);
        recycler_view.setAdapter(reportListAdapter);

        //calculate the total expense and income and show these two results
        incomeModel = new Select().from(IncomeModel.class).execute();
        for(int i=0; i<incomeModel.size(); i++){
            incomeSum = incomeSum + Integer.parseInt(incomeModel.get(i).getAmount());
        }
        expenseModels = new Select().from(ExpenseModel.class).execute();
        for(int i=0; i<expenseModels.size(); i++){
            expenseSum = expenseSum + Integer.parseInt(expenseModels.get(i).getAmount());
        }
        currentBalance = incomeSum - expenseSum;

        prepare_stage();
        report_details_prepare();


        recycler_view.addOnItemTouchListener(new RecyclerTouchListener(ReportActivity.this, recycler_view, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //Toast.makeText(ReportActivity.this, ""+position, Toast.LENGTH_SHORT).show();
                report_layout.setVisibility(View.GONE);
                details_layout.setVisibility(View.VISIBLE);

                load_details_data(position);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    public void load_details_data(int posRec){

        amount_text.setText(expenseModels.get(posRec).getAmount());
        payee_text.setText(expenseModels.get(posRec).getPayee());
        category_text.setText(expenseModels.get(posRec).getCategory());
        payment_method_text.setText(expenseModels.get(posRec).getPaymentMethod());
        payment_status_text.setText(expenseModels.get(posRec).getPaymentStatus());
        day_text.setText(expenseModels.get(posRec).getToDay()+"-");
        month_text.setText(expenseModels.get(posRec).getThisMonth()+"-");
        year_text.setText(expenseModels.get(posRec).getThisYear());

    }

    public void prepare_stage(){

        empty_list_show = (TextView) findViewById(R.id.empty_list_show);

        report_layout = (LinearLayout) findViewById(R.id.report_layout);
        details_layout = (LinearLayout) findViewById(R.id.details_layout);

        all_report = (LinearLayout) findViewById(R.id.all_report);
        all_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recycler_view.setVisibility(View.VISIBLE);
                empty_list_show.setVisibility(View.GONE);
                all_report.setBackgroundColor(Color.parseColor("#FFE0B2"));
                todays_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                monthly_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                weekly_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                Toast.makeText(ReportActivity.this, "All Expenses Shown !", Toast.LENGTH_SHORT).show();
                expenseModels = new Select().from(ExpenseModel.class).execute();
                reportListAdapter = new ReportListAdapter(expenseModels);
                recycler_view.setAdapter(reportListAdapter);
            }
        });

        todays_report = (LinearLayout) findViewById(R.id.todays_report);
        todays_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                todays_report.setBackgroundColor(Color.parseColor("#FFE0B2"));
                monthly_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                weekly_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                all_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                getTodaysReport();
            }
        });

        monthly_report = (LinearLayout) findViewById(R.id.monthly_report);
        monthly_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monthly_report.setBackgroundColor(Color.parseColor("#FFE0B2"));
                todays_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                weekly_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                all_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                getMonthlyReport();
            }
        });

        weekly_report = (LinearLayout) findViewById(R.id.weekly_report);
        weekly_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weekly_report.setBackgroundColor(Color.parseColor("#FFE0B2"));
                todays_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                monthly_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                all_report.setBackgroundColor(Color.parseColor("#ffffffff"));
                getWeeklyReport();
            }
        });

        income_total = (TextView) findViewById(R.id.income_total);
        expense_total = (TextView) findViewById(R.id.expense_total);
        cash_total = (TextView) findViewById(R.id.cash_total);

        income_total.setText(incomeSum+".00");
        expense_total.setText(expenseSum+".00");
        cash_total.setText(currentBalance+".00");

    }

    public void getTodaysReport(){

        List<ExpenseModel> sizeExpense = new ArrayList<>();
        sizeExpense = new Select().from(ExpenseModel.class).where("toDay = ?",toDay).execute();
        /*if(sizeExpense.isEmpty() || sizeExpense == null || sizeExpense.size() == 0){
            //recycler_view.setVisibility(View.GONE);
            //empty_list_show.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Your Today's Expense List is Empty !!", Toast.LENGTH_SHORT).show();
        }
        else {*/
            Toast.makeText(ReportActivity.this, "Today's Expenses List Shown !", Toast.LENGTH_SHORT).show();
            reportListAdapter = new ReportListAdapter(sizeExpense);
            recycler_view.setAdapter(reportListAdapter);
        //}

    }


    public void getWeeklyReport(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        calendar.add(Calendar.DAY_OF_YEAR, -7);
        int dy = calendar.get(Calendar.DAY_OF_MONTH);

        List<ExpenseModel> sizeExpense = new ArrayList<>();

       /* if(sizeExpense.isEmpty() || sizeExpense == null || sizeExpense.size() == 0){
            recycler_view.setVisibility(View.GONE);
            empty_list_show.setVisibility(View.VISIBLE);

        }
        else {*/
            Toast.makeText(ReportActivity.this, "Weekly Expenses List Shown !", Toast.LENGTH_SHORT).show();
            sizeExpense = new Select().from(ExpenseModel.class).where("toDay BETWEEN "+dy+" AND "+toDay).execute();
            reportListAdapter = new ReportListAdapter(sizeExpense);
            recycler_view.setAdapter(reportListAdapter);
       // }

    }

    public void getMonthlyReport(){
        Toast.makeText(ReportActivity.this, "Monthly Expenses List Shown !", Toast.LENGTH_SHORT).show();
        expenseModels = new Select().from(ExpenseModel.class).where("thisMonth = ?",thisMonth).execute();
        reportListAdapter = new ReportListAdapter(expenseModels);
        recycler_view.setAdapter(reportListAdapter);
    }

    public void report_details_prepare(){

        amount_text = (TextView) findViewById(R.id.amount_text);
        payee_text = (TextView) findViewById(R.id.payee_text);
        category_text = (TextView) findViewById(R.id.category_text);
        payment_method_text = (TextView) findViewById(R.id.payment_method_text);
        payment_status_text = (TextView) findViewById(R.id.payment_status_text);
        day_text = (TextView) findViewById(R.id.day_text);
        month_text= (TextView) findViewById(R.id.month_text);
        year_text = (TextView) findViewById(R.id.year_text);

    }

    @Override
    public void onBackPressed() {
        if(details_layout.getVisibility() == View.VISIBLE){
            details_layout.setVisibility(View.GONE);
            report_layout.setVisibility(View.VISIBLE);
        }else {
            finish();
        }
    }
}
