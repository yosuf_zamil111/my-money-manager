package com.sts.expensemanager2.UI;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.activeandroid.query.Select;
import com.sts.expensemanager2.Adapter.IncomeListAdapter;
import com.sts.expensemanager2.Model.IncomeModel;
import com.sts.expensemanager2.R;

import java.util.ArrayList;
import java.util.List;

public class IncomeListActivity extends AppCompatActivity {

    RecyclerView recycler_view_income_list;
    List<IncomeModel> incomeModelList = new ArrayList<>();
    IncomeListAdapter incomeListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_income_list);

        recycler_view_income_list = (RecyclerView) findViewById(R.id.recycler_view_income_list);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recycler_view_income_list.setLayoutManager(manager);

        incomeModelList = new Select().from(IncomeModel.class).execute();
        incomeListAdapter = new IncomeListAdapter(incomeModelList);
        recycler_view_income_list.setAdapter(incomeListAdapter);
    }
}
